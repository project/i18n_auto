<?php

define('I18N_AUTO_GOOGLE_TRANSLATE_MAIN_URL', 'http://google.com/translate');
define('I18N_AUTO_GOOGLE_TRANSLATE_TRANSLATOR_URL', 'http://google.com/translate_t');
define('I18N_AUTO_GOOGLE_TRANSLATE_TOS_URL', 'http://www.google.com/accounts/TOS');

/**
 * hook i18n_auto_TRANSLATOR_info
 * this returns information relevant to a specific 3rd party translator
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the translator
 *   'url' => the url to the main page for the translator
 *   'settings_description' => a description of the translator that will be posted in the admin settings form
 */
function i18n_auto_google_translate_info() {
  return array(
    'translator' => 'google_translate',
    'name' => t('Google Translate'),
    'url' => I18N_AUTO_GOOGLE_TRANSLATE_MAIN_URL,
    'settings_description' => t('These settings specifically affect translation from !google_translate. You should read their !tos before using their translation services.', array('!google_translate' => l(t('Google Translate'), I18N_AUTO_GOOGLE_TRANSLATE_MAIN_URL, array('target' => '_blank')), '!tos' => l(t('terms of service'), I18N_AUTO_GOOGLE_TRANSLATE_TOS_URL, array('target' => '_blank')))),
  );
}

/**
 * hook i18n_auto_TRANSLATOR_settings
 * this should return a subform to be added to the i18n_auto_settings() admin settings page.
 * note that a form field will already be provided, at $form['TRANSLATOR'] (such as $form['google_translate'])
 * so if you want specific translator settings within that field, you can add the elements to that form field.
 */
function i18n_auto_google_translate_settings() {
  $form = array();
  return $form;
}

function i18n_auto_google_translate_translate($text, $from = 'en', $to = 'es') {
  $data = "h1=en&ie=UTF8&text=$text";
  $result = drupal_http_request(I18N_AUTO_GOOGLE_TRANSLATE_TRANSLATOR_URL . '?langpair='. $from .'|'. $to, array(), 'POST', $data);
  preg_match('@\<div id\=result_box dir\=ltr\>([^\<]*)\</div\>@', $result->data, $match);
//   $form_exp = var_export($match, TRUE);
//   drupal_set_message("\$match = <pre>$form_exp</pre>");
// print "from: $from to: $to\n";
// print_r ($result);
// print_r ($match);
  return $match[1];
}

// call /translate_t?langpair=en|es
// put h1: en, ie: UTF8, text: $text
// put <input type=hidden name=hl value="en"><input type=hidden name=ie value="UTF8">
// results wrapped with:
// <div id=result_box dir=ltr>esto es una prueba</div>
