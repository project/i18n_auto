<?php

define('I18N_AUTO_BABELFISH_YAHOO_MAIN_URL', 'http://babelfish.yahoo.com/');

/**
 * hook i18n_auto_translator_info
 * this returns information relevant to a specific 3rd party translator
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the translator
 *   'url' => the url to the main page for the translator
 *   'settings_description' => a description of the translator that will be posted in the admin settings form
 */
function i18n_auto_babelfish_yahoo_info() {
  return array(
    'name' => t('BabelFish Yahoo!'),
    'url' => I18N_AUTO_BABELFISH_YAHOO_MAIN_URL,
    'settings_description' => t('These settings specifically affect translation from !babelfish_yahoo.', array('!babelfish_yahoo' => l('Babelfish Yahoo!', I18N_AUTO_BABELFISH_YAHOO_MAIN_URL, array('target' => '_blank')))),
  );
}
